package com.sigmotoa.codes.workshop;
/**
 * @author sigmotoa
 *
 * @version 1
 *
 * Geometric Exercises
 */


import static java.lang.Math.*;

public class Geometric {

//Calculate the area for a square using one side
public static int squareArea(int side)

{
    int area = side*side;
    System.out.println(area);
    return area;
}

//Calculate the area for a square using two sides
public static int squareArea(int sidea, int sideb)
{
    int area = sidea*sideb;
    System.out.println(area);
    return area;
}

//Calculate the area of circle with the radius
public static double circleArea(double radius)
{

    double radio = PI *2 * radius;
    System.out.println(radio);
    return radio;
}

//Calculate the perimeter of circle with the diameter
public static double circlePerimeter(int diameter)
{

    double  Perimeter =( 2 *Math.PI *diameter) /2;
    System.out.println(Perimeter);
    return Perimeter;
}

//Calculate the perimeter of square with a side
public static double squarePerimeter(double side) {
    {
        double perimetro = side * side;
        System.out.println(perimetro);
        return perimetro;
    }
}
//Calculate the volume of the sphere with the radius
public static double sphereVolume(double radius)
{
    double v = (4.0/3);
    double volumen = v * Math.PI * radius * radius * radius;
    System.out.println(volumen);
    return volumen;
}

//Calculate the area of regular pentagon with one side
public static float pentagonArea(int side)
{
    double area = (sqrt(5 * (5 + 2 * (sqrt(5)))) * side * side) / 4;
    System.out.print(area);
    return (float) area;
}

//Calculate the Hypotenuse with two cathetus
public static double calculateHypotenuse(double catA, double catB)
{
    double hipotenusa =Math.sqrt((catA*catA+catB*catB));
    System.out.println(hipotenusa);
    return hipotenusa;
}

}
