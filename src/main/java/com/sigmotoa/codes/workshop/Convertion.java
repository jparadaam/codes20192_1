package com.sigmotoa.codes.workshop;

/**
 * @author sigmotoa
 *
 * @version 1
 *
 * Convertion Exercises
 */
public class Convertion {

    //convert of units of Metric System

//Km to metters
    public static int kmToM1(double km) {
        double result = km * 1000;
        {
            System.out.println(result);
            return (int) result;
        }
    }

//Km to metters
    public static double kmTom(double km)
    {
        double result = ((double) km) * 1000;
        {
            System.out.println(result);
            return  result;
        }
    }
    
    //Km to cm
    public static double kmTocm(double km)
    {
        double result = ((double) km) * 100000;
        {
            System.out.println(result);
            return  result;
        }
    }
//milimetters to metters
    public static double mmTom(int mm)
    {
        double result = ((double) mm) / 1000;
        {
            System.out.println(result);
            return  result;
        }
    }
//convert of units of U.S Standard System

//convert miles to foot
    public static double milesToFoot(double miles)
    {
        {
            double result = ((double) miles) * 5280;
            {
                System.out.println(result);
                return  result;
            }
        }
    }

//convert yards to inches
    public static int yardToInch(int yard)
    {
        {
            double result = ((double) yard) * 3;
            {
                System.out.println(result);
                return (int) result;
            }
        }
    }
    
    //convert inches to miles
    public static double inchToMiles(double inch)

        {
            double result = ((double) inch) * 0.00001578283;
            {
                System.out.println(result);
                return ((float) result);
            }
        }

//convert foot to yards
    public static int footToYard(int foot)
    {
        double result = ((double) foot) * 0.333333;
        {
            System.out.println(result);
            return (int) result;
       }

    }

//convert Km to inches
    public static double kmToInch(String km)
    {
        double result = Float.parseFloat(km) * 39370.079;
        {
            System.out.println(result);
            return result;
        }
    }

//convert milimmeters to foots
    public static double mmToFoot(String mm)
    {
        double result = Float.parseFloat(mm) * 0.00328084;
        {
            System.out.println(result);
            return result;
        }
    }


//convert yards to cm    
    public static double yardToCm(String yard)
    {    {
        double result = Float.parseFloat(yard) * 91.44;
        {
            System.out.println(result);
            return result;
        }
    }}


}
